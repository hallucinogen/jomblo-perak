// Parse must be initiated first

function login(callback, errorCallback) {
  Parse.FacebookUtils.logIn(null, {
    success: function(user) {
      $.ajax({
        method: 'post',
        url: '/login',
        data: {
          sessionToken: user._sessionToken
        },
        success: function() {
          if (callback) {
            callback();
          }
        },
        error: function(user, error) {
          if (errorCallback) {
            errorCallback(error);
          }
        }
      });
    },
    error: function(user, error) {
      if (errorCallback) {
        errorCallback(error);
      }
    }
  });
}

function logout(callback, errorCallback) {
  $.ajax({
    method: 'post',
    url: '/logout',
    data: {
      sessionToken: Parse.User.current()._sessionToken
    },
    success: function() {
      Parse.User.logOut();
      if (callback) {
        callback();
      }
    },
    error: function(user, error) {
      if (errorCallback) {
        errorCallback(error);
      }
    }
  });
}
