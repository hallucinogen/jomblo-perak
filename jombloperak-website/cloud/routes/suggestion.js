var Victim = Parse.Object.extend('Victim');
var Suggestion = Parse.Object.extend('Suggestion');

Parse.Cloud.beforeSave('Suggestion', function(req, res) {
  var object = req.object;
  var error = '';

  if (!object.get('fbId')) {
    error += 'Anda belum merekomendasikan orang\n';
  }
  if (!object.get('description')) {
    error += 'Anda belum menuliskan mengapa anda merekomendasikan orang ini\n';
  }
  if (!object.get('victim') || !object.get('victim').id) {
    error += 'Anda harus memilih target\n';
  }
  if (!object.get('owner')) {
    error += 'Anda belum login\n';
  }
  if (error.length) {
    res.error(error);
    return;
  }

  var query = new Parse.Query(Suggestion);
  query.equalTo('fbId', object.get('fbId'));
  query.equalTo('victim', object.get('victim'));
  query.first({
    success: function(obj) {
      if (obj) {
        res.error('Sudah ada yang merekomendasikan orang ini.');
      } else {
        res.success();
      }
    }
  });
});

exports.doAddSuggestion = function(req, res) {
  var suggestion = new Suggestion();
  suggestion.set('fbId', req.body.fbid);
  suggestion.set('description', req.body.description);
  suggestion.set('victim', new Victim({id: req.body.victimId})._toPointer());

  var currentUser = Parse.User.current();
  if (currentUser) {
    suggestion.set('owner', currentUser);

    // Set permission
    var acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setWriteAccess(currentUser, true);
    suggestion.setACL(acl);
  }

  suggestion.save().then(function(object) {
    res.send(object);
  }, function(err) {
    res.send(400, err);
  });
};

// Get suggestion will be called by AJAX
exports.getSuggestions = function(req,res) {
  var victimId = req.params.victimId;
  var page = req.params.page || 0;
  var query = new Parse.Query(Suggestion);
  query.equalTo('victim', new Victim({id: victimId})._toPointer());
  query.limit(3);
  query.skip(page * 3); 

  query.find({
    success: function(results) {
      res.send(results);
    },
    error: function(error) {
      res.send('Not found');
    }
  });
};
