var Victim = Parse.Object.extend('Victim');

Parse.Cloud.beforeSave('Victim', function(req, res) {
  var object = req.object;
  var error = '';

  if (!object.get('name')) {
    error += 'Masukkan nama\n';
  }
  if (!object.get('birthDate')) {
    error += 'Masukkan tanggal lahir\n';
  }
  if (error.length) {
    res.error(error);
    return;
  }

  if (!object.get('fbId')) {
    var query = new Parse.Query(Victim);
    query.equalTo('vanityUrl', object.get('vanityUrl'));
    query.first({
      success: function(obj) {
        if (obj) {
          res.error("Sudah ada orang dengan nama itu. Apakah maksud anda: <a href='/" + obj.get('vanityUrl') + "'>" + obj.get('name') + "</a>");
        } else {
          res.success();
        }
      }
    });
  } else {
    res.success();
  }
});

var formatDateToCircular = function(date) {
  return date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate() + ' 00:00:00';
}

exports.addVictim = function(req, res) {
  res.render('add_victim', {
    title: 'Countdown menuju 25 tahun forever alone'
  });  
};

exports.doAddVictim = function(req, res) {
  // TODO: when error, flash error instead of doing page request

  var isFb = req.body.fb_is_login;
  var name = isFb ? req.body.fb_name : req.body.name;
  var birthDate = req.body.birth_date;
  var fbId = isFb ? req.body.fb_id : null;

  if (isFb) {
    doAddVictimFb(res, name, birthDate, fbId);
  } else {
    doAddVictimStatic(res, name, birthDate);
  }
};

exports.getCountdown = function(req, res) {
  var query = new Parse.Query(Victim);
  query.equalTo('vanityUrl', req.params.vanityUrl);

  query.first({
    success: function(victim) {
      if (!victim) {
        res.send('Not found');
        return;
      }
      doRedirectCountdown(false, res, victim);
    },
    error: function(error) {
      res.send("Not found");
    }
  });
};

exports.getCountdownFb = function(req, res) {
  var query = new Parse.Query(Victim);
  query.equalTo("fbId", req.params.fbId);

  query.first({
    success: function(victim) {
      if (!victim) {
        res.send("Not found");
        return;
      }

      doRedirectCountdown(true, res, victim, req.headers.host);
    },
    error: function(error) {
      res.send('Not found');
    }
  });
};

exports.getStaticCountdown = function(req, res) {
  res.render('static_countdown', {
    title: 'Countdown menuju Jomblo Perak',
    name: req.params.name,
    month: req.params.month,
    date: req.params.date,
    year: req.params.year
  });
};

// Private functions here
function doAddVictimFb(res, name, birthDate, fbId) {
  var query = new Parse.Query(Victim);
  query.equalTo("fbId", fbId);
  query.first({
    success: function(victim) {
      if (victim) {
        res.send("Teman anda sudah punya Countdown: <a href='/" + fbId + "'>" + victim.get('name') + "</a>");
        return;
      }

      var newVictim = new Victim();

      newVictim.set('name', name);
      newVictim.set('birthDate', birthDate);
      newVictim.set('vanityUrl', null);
      newVictim.set('fbId', fbId);
      doSaveVictim(true, res, newVictim);
    },
    error: function(error) {
      res.error(error);
    }
  });
};

function doAddVictimStatic(res, name, birthDate) {
  // make default vanity url
  var vanityUrl = name.toLowerCase().split(" ").join(".");
  var query = new Parse.Query(Victim);
  query.equalTo("vanityUrl", vanityUrl);
  query.first({
    success: function(victim) {
      if (victim) {
        res.send("Sudah ada orang dengan nama itu. Apakah maksud anda: <a href='/static/" + victim.get('vanityUrl') + "'>" + victim.get('name') + "</a>");
        return;
      }

      var newVictim = new Victim();
      newVictim.set('name', name);
      newVictim.set('birthDate', birthDate);
      newVictim.set('vanityUrl', vanityUrl);
      newVictim.set('fbId', null);
      doSaveVictim(false, res, newVictim);
    },
    error: function(error) {
      res.error(error);
    }
  });
};

function doSaveVictim(isFb, res, victim) {
  var currentUser = Parse.User.current();
  if (currentUser) {
    victim.set('owner', currentUser);

    // Set permission
    var acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setWriteAccess(currentUser, true);
    victim.setACL(acl);
  }

  victim.save().then(function(object) {
    var redirectUrl = (isFb?'/':'/static/') + object.get(isFb?'fbId':'vanityUrl');
    res.redirect(redirectUrl);
  }, function(error) {
    res.send("Terjadi kesalahan");
  });
};

function doRedirectCountdown(isFb, res, victim, baseUrl) {
  var startDate = new Date(victim.get("birthDate"));
  var endDate = new Date(startDate);
  endDate.setFullYear(endDate.getFullYear() + 25);

  // Manually format to string
  startDate = formatDateToCircular(startDate);
  endDate = formatDateToCircular(endDate);
  res.render('countdown', {
    title: 'Countdown menuju Jomblo Perak',
    startDate: startDate,
    endDate: endDate,
    name: victim.get('name'),
    id: victim.id,
    fbId: victim.get('fbId'),
    url: (isFb?'':'static/') + victim.get(isFb?'fbId':'vanityUrl'),
    baseUrl: baseUrl
  });
};
