var Suggestion = Parse.Object.extend('Suggestion');
var Comment = Parse.Object.extend('Comment');

Parse.Cloud.beforeSave('Comment', function(req, res) {
  var object = req.object;
  var valid = true;

  if (!object.get('author')) {
    res.error('Tidak ada pengirim');
    valid = false;
  }
  if (!object.get('message')) {
    res.error('Tidak ada pesan');
    valid = false;
  }
  if (!object.get('suggestion')) {
    res.error('Tidak ada target');
    valid = false;
  }

  if (!valid) return;

  res.success();
});

exports.doAddComment = function(req, res) {
  var comment = new Comment();
  comment.set('message', req.body.message);
  comment.set('suggestion', new Suggestion({id: req.body.suggestionId}));

  currentUser = Parse.User.current();
  if (currentUser) {
    comment.set('author', currentUser);

    // Set permission
    var acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setWriteAccess(currentUser, true);
    comment.setACL(acl);
  }

  comment.save().then(function(object) {
    res.send(object);
  }, function(error) {
    res.send(400, error);
  });
};

// Get comment will be called by AJAX
exports.getComments = function(req,res) {
  var suggestionId = req.params.suggestionId;
  var page = req.params.page || 0;
  var query = new Parse.Query(Comment);
  query.equalTo('suggestion', new Suggestion({id: suggestionId}));
  query.include('author');
  query.limit(10);
  query.skip(page * 10); 

  query.find({
    success: function(results) {
      res.send(results);
    },
    error: function(error) {
      res.send('Not found');
    }
  });
};
