var express = require('express');
var expressLayouts = require('cloud/express-layouts');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
var parseExpressCookieSession = require('parse-express-cookie-session');

var user = require('cloud/routes/user');
var suggestion = require('cloud/routes/suggestion');
var comment = require('cloud/routes/comment');


var app = express();

// Configure the app
app.set('views', 'cloud/views');
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(parseExpressHttpsRedirect());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('_awesomebeat'));
app.use(parseExpressCookieSession({
  key: 'jomblo.sess',
  cookie: {
    maxAge: 3600000 * 24 * 30
  }
}));

app.locals.parseApplicationId = 'mhpCtyuEz8uRmS0gPfJnUF9eFssRaqOaX0l57JPy';
app.locals.parseJavascriptKey = 'bFZhs53Hl8RPOilYhCBTHI1ORO8FSrm4LsBlQXm3';
app.locals.facebookApplicationId = '230514897145786';

// Setup underscore to be available in all templates
app.locals._ = require('underscore');

// TODO: move this to user.doLogin and user.doLogout
// Define all the endpoints
app.post('/login', function(req, res) {
  Parse.User.become(req.body.sessionToken, {
    success: function(user) {
      res.send(200);
    },
    error: function(user, error) {
      res.send(400);
    }
  });
});
app.post('/logout', function(req, res) {
  Parse.User.logOut();
  res.send(200);
});

app.get('/', user.addVictim);
app.post('/', user.doAddVictim);
app.post('/suggestion', suggestion.doAddSuggestion);
app.get('/suggestion/:victimId', suggestion.getSuggestions);
app.post('/comment', comment.doAddComment);
app.get('/comment/:suggestionId', comment.getComments);
app.get('/:fbId', user.getCountdownFb);
app.get('/static/:vanityUrl', user.getCountdown);


app.listen(); 
